package com.tp.java2;

public class Moyenne
{
	private double moyenne = 0;
	private int nbVal = 1;
	
	
	
	public int getNbVal()
	{
		return nbVal;
	}

	public void setNbVal(int nbVal)
	{
		this.nbVal = nbVal;
	}

	public double getMoyenne()
	{
		return moyenne;
	}
	
	public void ajout(double val)
	{
		this.moyenne = this.moyenne + val;
	}
	
	public void moyennage()
	{
		this.moyenne = this.moyenne/this.nbVal;
	}
	
}
