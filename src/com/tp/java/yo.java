package com.tp.java;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.ListIterator;
//import java.text.NumberFormat;
//import java.util.Scanner;

//import com.tp.java2.Addition;
//import com.tp.java2.Moyenne;
//import com.tp.java2.Voiture;

public class yo
{
	private String nom;
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNom()
	{
		return nom;
	}
	
	
	
	public static void main(String[] args)
	{
		//print "hello world" in the terminal window.
		/*System.out.println("yo");
		
		yo myName = new yo();
		myName.setNom("machin");
		System.out.println("vous avez entr� le nom " + myName.getNom());
		
		Voiture maVoiture = new Voiture();
		maVoiture.setMarque("Inston martin");
		System.out.println("vous avez choisi la marque " + maVoiture.getMarque());*/
		
		//NumberFormat format=Number.getInstance();
		
		/*Scanner sc = new Scanner(System.in);
		Moyenne res = new Moyenne();
		
		double i = 0;
		int j = 0;
		double max = 0;
		double min = 100;
		while(i != -1)
		{
			System.out.println("veuillez entrer une note entre 0 et 20.");
			i = sc.nextDouble();
			//if(i == -1) return;
			if(i == -1)
			{
				System.out.println("toutes les notes sont entr�es !");
			}
			else if(i < 0 || i > 20)
			{
				System.out.println("cette valeur n'est pas correcte !");
			}
			else
			{
				j++;
				res.ajout(i);
				if(i > max) max = i;
				if(i < min) min = i;
			}
			
		}
		
		if(j == 0)
		{
			System.out.println("erreur ! vous n'avez entr� aucune valeur !");
		}
		else if(j == 1)
		{
			System.out.println("vous n'avez entr� qu'une seule valeur : " + res.getMoyenne());
		}
		else if(j == 2)
		{
			res.setNbVal(j);
			res.moyennage();
			System.out.printf("voici la moyenne des deux notes que vous avez entr�es : %.2f", res.getMoyenne());
		}
		else
		{
			res.setNbVal(j-2);
			res.ajout(-max);
			res.ajout(-min);
			res.moyennage();
			System.out.println("la note " + min + " et la note " + max + " ont �t� retir�es");
			System.out.printf("voici la moyenne des " + res.getNbVal() + " notes que vous avez entr�es sans prendre en compte le min et le max : %.2f", res.getMoyenne());
		}
		
		//System.out.println("coucou");
		sc.close();*/
		
		/*List l = new LinkedList();
		l.add(12);
		l.add("toto ! !");
		l.add(12.20f);
		int j = l.size();
		
		for(int i = 0; i < j; i++)
		{
			System.out.println("�lement � l'index " + i + " = " + l.get(i));
		}
		
		for(int i = 0; i < j; i++)
		{
			l.remove(0);
		}

		System.out.println("nombre d'�l�ments restant : " + l.size());*/
		
		/*Hashtable<Integer, String> ht = new Hashtable<Integer, String>();
		ht.put(50,  "printemps");
		ht.put(8, "�t�");
		ht.put(4, "automne");
		ht.put(2, "hiver");
		
		Enumeration<String> e = ht.elements();
		
		while(e.hasMoreElements()) System.out.println(e.nextElement());
		
		System.out.println(ht.get(2));*/
		
		HashSet<Comparable> hs = new HashSet<Comparable>();
		hs.add("toto");
		hs.add("12");
		hs.add('d');
		hs.add('d');
		
		Iterator<Comparable> it = hs.iterator();
		
		while(it.hasNext()) System.out.println(it.next());
		
		System.out.println("\nparcours avec un tableau d'objet");
		System.out.println("--------------------------------");
		
		Object[] obj = hs.toArray();
		for(Object o : obj) System.out.println(o);
		
		double[] machin = {1, 1.5, 44};
		machin[0] = 1.2;
		machin[1] = 4;
		machin[2] = 8;
		
		for(int i = 0; i < 3; i++) System.out.println(machin[i]);
	}
}