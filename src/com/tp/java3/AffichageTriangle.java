package com.tp.java3;

public class AffichageTriangle
{
	public void affichage(int num, String motif)
	{
		boolean tropGrand = false;
		for(int i = 1; i <= num; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(j*motif.length()>196 && tropGrand == false) tropGrand = true;
				
				if(tropGrand == false) System.out.print(motif);
			}
			if(tropGrand == true) System.out.print("le motif prend trop de place !");
			System.out.print("\n");
		}
	}
}
