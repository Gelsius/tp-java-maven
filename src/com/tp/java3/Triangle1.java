package com.tp.java3;

public class Triangle1 {
	
	public static void help()
	{
		System.out.println("pour sp�cifier la taille, veuillez entrer : -taille <valeur>.");
		System.out.println("pour sp�cifier un motif, veuillez entrer : -motif <motif>.");
		System.out.println("pour ce teste, veuillez entrer : -help !");
	}
	
	public static void main(String[] args)
	{
		int num = 0;
		int n = 0;
		String motif = "*";
		
		AffichageTriangle tri = new AffichageTriangle();
		
		boolean verifT = false;
		boolean verifM = false;
		while(n<args.length && (verifT == false || verifM == false))
		{
			if (args[n].equals("-taille"))
			{
				if(n+1 == args.length)
				{
					System.out.println("la taille sp�cifi� n'a pas �t� trouv�e !");
					help();
					return;
				}
				if(verifT == false)
				{
					verifT = true;
					try
					{
						num = Integer.parseInt(args[n+1]);
					}
				    catch(NumberFormatException nfe)
					{
				    	System.out.println("vous n'avez pas entrer une information viable");
				    	help();
				    	return;
				    }
				}
			}
			else if (args[n].equals("-motif"))
			{
				if(n+1 == args.length)
				{
					System.out.println("le motif sp�cifi� n'a pas �t� trouv�");
					help();
					return;
				}
				
				if(verifM == false)
				{
					motif = args[n+1];
					verifM = true;
				}
			}
			else if (args[n].equals("-help"))
			{
				help();
				return;
			}
			
			n++;
		}
		
		if(verifT == false)
		{
			System.out.println("la taille n'est pas sp�cifier ! pour sp�cifier une taille, mettez -taille suivi de la taille que vous voulez mettre.");
			help();
			return;
		}
		
		
		if(verifM == false)
		{
			System.out.println("� cause de l'absence de motif, le programme affichera * !");
		}
	    
		tri.affichage(num, motif);
		
	}
	
}
